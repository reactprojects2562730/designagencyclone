*****Project Information*****

-This project is made by using pure HTML & CSS.
-For the Icons I have used fontawesome.com website.
-For fonts used Google Fonts: Poppins (Regular 400, Bold 700) & Roboto (Regular 400, Bold 700).
-To download the images and logoes in the website I used one chrome extesion called 'CSS Peeper'.
-To know css details 'css viewer' extension used.
